enum ApiRouter {

    // MARK: - Cases

    case lenta
    case gazeta

    // MARK: - Property list

    private var lentaPath: String { "https://lenta.ru/rss/last24" }
    private var gazetaPath: String { "https://www.gazeta.ru/export/rss/lastnews.xml" }

    var path: String {
        switch self {
        case .lenta:
            return lentaPath
        case .gazeta:
            return gazetaPath
        }
    }
}
