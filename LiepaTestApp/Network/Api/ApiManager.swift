import Foundation
import SWXMLHash

protocol ApiManagerRSS {

    typealias LentaBlock = ([Lenta]) -> Void
    typealias GazetaBlock = ([Gazeta]) -> Void
    typealias ErrorBlock = (Error) -> Void

    func getLentaNews(success: @escaping LentaBlock, failure: @escaping ErrorBlock)
    func getGazetaNews(success: @escaping GazetaBlock, failure: @escaping ErrorBlock)
}

extension ApiManager: ApiManagerRSS {
    func getLentaNews(success: @escaping LentaBlock, failure: @escaping ErrorBlock) {
        getData(for: .lenta, success: success, failure: failure)
    }

    func getGazetaNews(success: @escaping GazetaBlock, failure: @escaping ErrorBlock) {
        getData(for: .gazeta, success: success, failure: failure)
    }
}

final class ApiManager {

    private typealias DataBlock = (Data) -> Void

    static let shared = ApiManager()

    // MARK: - Private methods

    private func getData(by urlPath: String, success: @escaping DataBlock, failure: @escaping ErrorBlock) {

        guard let url = URL(string: urlPath) else {
            failure(RestError.wrongURL(path: urlPath))
            return
        }

        URLSession.shared.dataTask(with: url, completionHandler: { data, response, error in
            
            if let error = error {
                failure(error)
                return
            }
            
            if let response = response as? HTTPURLResponse, (200..<300).contains(response.statusCode), let data = data {
                success(data)
            } else {
                failure(RestError.someRequestError)
            }
        }).resume()
    }

    // MARK: - Public methods

    func getData<T: XMLIndexerDeserializable>(for apiRouter: ApiRouter, success: @escaping ([T]) -> Void, failure: @escaping ErrorBlock) {
        getData(by: apiRouter.path, success: { data in
            do {
                let xml = SWXMLHash.parse(data)
                let objects: [T] = try xml["rss"]["channel"]["item"].value()
                success(objects)
            } catch {
                return failure(RestError.decodingError)
            }
        }, failure: failure)
    }
}
