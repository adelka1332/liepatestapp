import SWXMLHash

struct Lenta: XMLIndexerDeserializable {

    var title: String
    var author: String
    var description: String
    var pubDate: Date
    var imageUrl: URL

    static func deserialize(_ node: XMLIndexer) throws -> Lenta {
        var model = try Lenta(title: node["title"].value(),
                              author: "Lenta.ru",
                              description: node["description"].value(),
                              pubDate: try node["pubDate"].value(),
                              imageUrl: try node["enclosure"].value(ofAttribute: "url"))
        model.description = model.description.trimmingCharacters(in: .whitespacesAndNewlines)
        return model
    }
}
