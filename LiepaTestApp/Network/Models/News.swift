import Foundation

struct News {
    var title: String
    var author: String
    var description: String
    var pubDate: Date
    var imageUrl: URL

    var isSelected: Bool = false
}
