import SWXMLHash

struct Gazeta: XMLIndexerDeserializable {

    var title: String
    var author: String
    var pubDate: Date
    var description: String
    var imageUrl: URL

    static func deserialize(_ node: XMLIndexer) throws -> Gazeta {
        return try Gazeta(title: node["title"].value(),
                          author: node["author"].value(),
                          pubDate: try node["pubDate"].value(),
                          description: node["description"].value(),
                          imageUrl: try node["enclosure"].value(ofAttribute: "url"))
    }
}
