import Foundation

enum RestError: LocalizedError {
    
    // MARK: - Cases
    
    case someRequestError
    case wrongURL(path: String)
    case decodingError
    
    // MARK: - Property list

    var errorDescription: String {
        switch self {
        case .someRequestError:
            return "Something on server goes wrong"
        case .wrongURL(let path):
            return "Wrong URL path: \(path)"
        case .decodingError:
            return "Some decoding error"
        }
    }
}
