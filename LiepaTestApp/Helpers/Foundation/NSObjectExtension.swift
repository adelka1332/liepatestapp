import Foundation

extension NSObject {

    static var nameOfClass: String {
        return String(describing: self)
    }
}
