import UIKit

extension UIView {

    func showProgressHud(color: UIColor = .gray,
                         backgroundColor: UIColor = .white,
                         position: CGPoint = .zero,
                         shouldBlockOtherViews: Bool = true,
                         topAnchor: CGFloat? = nil,
                         bottomAnchor: CGFloat? = nil,
                         viewIndex: Int = 1) {

        guard subviews.contains(where: { $0 is ProgressHUD && $0.tag == viewIndex }) == false else {
            return
        }

        let progressHUD = ProgressHUD(color: color,
                                      backgroundColor: backgroundColor,
                                      position: position,
                                      topAnchor: topAnchor,
                                      bottomAnchor: bottomAnchor)
        progressHUD.alpha = 0
        progressHUD.layer.zPosition = 101
        progressHUD.tag = viewIndex
        addSubview(progressHUD)
        progressHUD.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            progressHUD.leftAnchor.constraint(equalTo: self.leftAnchor),
            progressHUD.topAnchor.constraint(equalTo: self.topAnchor),
            progressHUD.rightAnchor.constraint(equalTo: self.rightAnchor),
            progressHUD.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])

        if !shouldBlockOtherViews {
            progressHUD.isUserInteractionEnabled = false
        }

        let animator = UIViewPropertyAnimator(duration: 0.125,
                                              curve: .linear) { [weak progressHUD] in
            progressHUD?.alpha = 1.0
        }
        animator.startAnimation()
    }

    func hideProgressHud(viewIndex: Int = 1) {
        guard let progressHUD = subviews.first(where: { $0 is ProgressHUD && $0.tag == viewIndex }) else {
                return
        }

        let animator = UIViewPropertyAnimator(duration: 0.125,
                                              curve: .linear) { [weak progressHUD] in
            progressHUD?.alpha = 0
        }

        animator.addCompletion { [weak progressHUD] _ in
            progressHUD?.removeFromSuperview()
        }

        animator.startAnimation()

    }
}
