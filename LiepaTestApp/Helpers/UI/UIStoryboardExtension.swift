import UIKit

extension UIStoryboard {

    static func screen<T: UIViewController>(_ screenType: T.Type) -> T {
        let screenName = screenType.nameOfClass
        let story = UIStoryboard(name: screenName, bundle: nil)

        guard let vc = story.instantiateViewController(withIdentifier: screenName) as? T else {
            fatalError("No ViewController in \(screenName).storyboard with id: \(screenName)")
        }
        return vc
    }
}
