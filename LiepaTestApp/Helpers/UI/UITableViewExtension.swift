import UIKit.UITableView

extension UITableView {

    func registerWithNib<T: UITableViewCell>(cellType: T.Type, bundle: Bundle? = nil) {
        let nib = UINib(nibName: cellType.nameOfClass, bundle: bundle)
        register(nib, forCellReuseIdentifier: cellType.nameOfClass)
    }

    func dequeueReusableCell<T: UITableViewCell>(cell: T.Type, for indexPath: IndexPath) -> T {
        guard let reusableCell = dequeueReusableCell(withIdentifier: cell.nameOfClass, for: indexPath) as? T else {
            fatalError("Cell \(cell.nameOfClass) not found")
        }
        return reusableCell
    }
}
