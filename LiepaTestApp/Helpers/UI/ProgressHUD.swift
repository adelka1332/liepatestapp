//
//  ProgressHUD.swift
//  LiepaTestApp
//
//  Created by Адель Багаутдинов on 12.10.2020.
//

import UIKit

final class ProgressHUD: UIView {

    // MARK: - Public Properties

    let spinner: UIActivityIndicatorView
    let animationWidthMultiplier: CGFloat = 0.8

   // MARK: - Lifecycle

    init(color: UIColor, backgroundColor: UIColor, position: CGPoint, topAnchor: CGFloat? = nil, bottomAnchor: CGFloat? = nil) {

        spinner = UIActivityIndicatorView()
        spinner.style = .medium
        spinner.color = .gray

        super.init(frame: .zero)
        addSubview(spinner)

        self.backgroundColor = backgroundColor
        if let topAnchor = topAnchor {
            setupConstraints(topAnchor: topAnchor)
        } else if let bottomAnchor = bottomAnchor {
            setupConstraints(bottomAnchor: bottomAnchor)
        } else {
            setupConstraints(animationOffset: position)
        }
        spinner.startAnimating()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    private func setupConstraints(animationOffset: CGPoint) {
        setupBaseConstraints()
        NSLayoutConstraint.activate([
            spinner.centerXAnchor.constraint(equalTo: centerXAnchor, constant: animationOffset.x),
            spinner.centerYAnchor.constraint(equalTo: centerYAnchor, constant: animationOffset.y)
        ])
    }

    private func setupConstraints(topAnchor: CGFloat) {
        setupBaseConstraints()
        NSLayoutConstraint.activate([
            spinner.centerXAnchor.constraint(equalTo: centerXAnchor, constant: CGPoint.zero.x),
            spinner.topAnchor.constraint(equalTo: self.topAnchor, constant: topAnchor)
        ])
    }

    private func setupConstraints(bottomAnchor: CGFloat) {
        setupBaseConstraints()
        NSLayoutConstraint.activate([
            spinner.centerXAnchor.constraint(equalTo: centerXAnchor, constant: CGPoint.zero.x),
            spinner.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: bottomAnchor)
        ])
    }

    private func setupBaseConstraints() {
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.widthAnchor.constraint(equalTo: widthAnchor,
                                       multiplier: animationWidthMultiplier).isActive = true
    }
}
