import UIKit

// MARK: - Label

extension UILabel {

    func apply(style: UIStyle<UILabel>) {
        style.apply(for: self)
    }
}
