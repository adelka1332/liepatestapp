import SWXMLHash

extension URL: XMLElementDeserializable, XMLAttributeDeserializable {

    public static func deserialize(_ element: XMLElement) throws -> URL {
        let url = URL(string: element.text)

        guard let validUrl = url else {
            throw XMLDeserializationError.typeConversionFailed(type: "URL", element: element)
        }
        return validUrl
    }

    public static func deserialize(_ attribute: XMLAttribute) throws -> URL {
        let url = URL(string: attribute.text)

        guard let validUrl = url else {
            throw XMLDeserializationError.attributeDeserializationFailed(type: "URL", attribute: attribute)
        }

        return validUrl
    }
}
