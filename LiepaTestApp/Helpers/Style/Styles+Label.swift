import UIKit

extension UIStyle {

    static var headingLabelStyle: UIStyle<UILabel> {
        return HeadingLabelStyle()
    }

    static var grayLabelStyle: UIStyle<UILabel> {
        return GrayLabelStyle()
    }
}
