import UIKit

final class GrayLabelStyle: UIStyle<UILabel> {

    override func apply(for control: UILabel) {
        control.font = UIFont.systemFont(ofSize: 17.0)
        control.textColor = .gray
        control.numberOfLines = 3
    }
}
