import UIKit

final class HeadingLabelStyle: UIStyle<UILabel> {

    override func apply(for control: UILabel) {
        control.font = UIFont.systemFont(ofSize: 17.0)
        control.textColor = .black
        control.numberOfLines = 3
    }
}
