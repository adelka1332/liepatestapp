import UIKit

protocol UIStylable {

    associatedtype Control: UIView

    func apply(style: UIStyle<Control>)
}

protocol UIStyleProtocol {

    associatedtype Control: UIView

    func apply(for: Control)
}

class UIStyle<Control: UIView>: UIStyleProtocol {

    init() { }

    func apply(for control: Control) { }
}
