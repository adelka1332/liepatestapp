import Foundation

enum SettingsService {
    
    // MARK: - Nested

    private enum Key {
        private static let service = String(describing: SettingsService.self)

        static let newsSources = service + ".news-sources"
        static let updateFrequency = service + ".update-frequency"
    }

    enum TimeSeconds {
        static var oneMin = 60
        static var fiveMin = 300
        static var tenMin = 600
    }

    enum FeedSources: String {
        case lentaru
        case gazetaru
    }

    // MARK: - Constants

    private static let userDefaults = UserDefaults.standard

    static var updateFrequency: Double {
        get { userDefaults.double(forKey: Key.updateFrequency) }
        set { userDefaults.setValue(newValue, forKey: Key.updateFrequency) }
    }

    static var feedSources: [String] {
        get { userDefaults.array(forKey: Key.newsSources) as? [String] ?? [] }
        set { userDefaults.setValue(newValue, forKey: Key.newsSources) }
    }

    static func fillDefaultValues() {
        SettingsService.feedSources = [SettingsService.FeedSources.lentaru.rawValue,
                                       SettingsService.FeedSources.gazetaru.rawValue]
    }
}
