import Foundation

final class NewsTableViewCellPresenter: NewsTableViewCellOutput {

    weak var cell: NewsTableViewCellInput?

    var heading: String { news.title }
    var imageUrl: URL { news.imageUrl }
    var author: String { news.author }
    var pubDate: Date { news.pubDate }

    var isSelected: Bool {
        get { news.isSelected }
        set { news.isSelected = newValue }
    }

    private var news: News

    init(news: News) {
        self.news = news
    }

    func getModel() -> News {
        return news
    }

    func selected() {
        news.isSelected = true
        cell?.select()
    }
}
