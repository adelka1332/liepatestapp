import UIKit
import Kingfisher

class NewsTableViewCell: UITableViewCell, NewsTableViewCellInput {

    private enum Constants {
        static let dateFormat = "MMM d, H:mm:ss"
    }

    @IBOutlet private weak var newsImage: UIImageView!
    @IBOutlet private weak var headingLabel: UILabel!
    @IBOutlet private weak var author: UILabel!

    // MARK: - Property list

    var presenter: NewsTableViewCellOutput? {
        willSet {
            presenter?.cell = nil
        }
        didSet {
            presenter?.cell = self
            configure()
        }
    }

    // MARK: - Private methods

    private func configure() {
        guard let presenter = presenter else { return }

        headingLabel.text = presenter.heading
        author.text = presenter.author + " : " + presenter.pubDate.toString(format: Constants.dateFormat)
        headingLabel.apply(style: presenter.isSelected ? .grayLabelStyle : .headingLabelStyle)
        author.apply(style: presenter.isSelected ? .grayLabelStyle : .headingLabelStyle)
        newsImage.kf.setImage(with: presenter.imageUrl)
    }

    //MARK: - NewsTableViewCellInput

    func select() {
        headingLabel.apply(style: .grayLabelStyle)
        author.apply(style: .grayLabelStyle)
    }
}
