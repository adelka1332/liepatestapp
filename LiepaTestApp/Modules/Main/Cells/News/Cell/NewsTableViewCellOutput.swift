import Foundation

protocol NewsTableViewCellOutput: CellOutputProtocol {
    var cell: NewsTableViewCellInput? { get set }

    var heading: String { get }
    var imageUrl: URL { get }
    var author: String { get }
    var pubDate: Date { get }
    var isSelected: Bool { get set }

    func selected()
}
