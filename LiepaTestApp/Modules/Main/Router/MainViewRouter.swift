import UIKit

final class MainViewRouter {

    // MARK: - Property list

    weak var transitionHandler: UIViewController!
}

//MARK: - MainViewRouterInput

extension MainViewRouter: MainViewRouterInput {

    func openDetails(with data: News) {
        let detailsVC = UIStoryboard.screen(DetailsViewController.self)
        detailsVC.presenter.set(data: data)
        transitionHandler.show(detailsVC, sender: nil)
    }
}
