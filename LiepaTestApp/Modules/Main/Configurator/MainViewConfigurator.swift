import UIKit

final class MainViewConfigurator {

    static func configureModule(with viewController: MainViewController) {
        let router = MainViewRouter()
        router.transitionHandler = viewController

        let interactor = MainViewInteractor()
        interactor.apiManager = ApiManager.shared
        interactor.dispatchGroup = DispatchGroup()

        let presenter = MainViewPresenter()
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router

        interactor.presenter = presenter

        viewController.presenter = presenter
    }
}
