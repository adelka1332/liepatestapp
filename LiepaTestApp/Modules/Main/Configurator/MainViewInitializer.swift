import UIKit

final class MainViewInitializer: NSObject {

    // MARK: - Property list

    @IBOutlet private weak var mainViewController: MainViewController!

    // MARK: - Overrides

    override func awakeFromNib() {
        MainViewConfigurator.configureModule(with: mainViewController)
    }
}
