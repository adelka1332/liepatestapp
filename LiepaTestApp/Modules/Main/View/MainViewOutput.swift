import Foundation

protocol MainViewOutput {
    func viewDidLoad()
    func pullToRefresh()
    func numberOfRowsInSection() -> Int
    func rowSelected(at indexPath: IndexPath)
    func presenterForCell(at indexPath: IndexPath) -> CellOutputProtocol
}
