import UIKit

final class MainViewController: UIViewController, MainViewInput {

    private enum Constants {
        static let navBarTitle = "RSS"
    }

    // MARK: - IBOutlets

    @IBOutlet private weak var tableView: UITableView!

    // MARK: - Internal Properties

    var presenter: MainViewOutput!

    // MARK: - Private properties

    private let refreshControl = UIRefreshControl()

    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        configureAppearance()
        view.showProgressHud()
        presenter.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
    }

    //MARK: - MainViewInput

    func reloadTable() {
        view.hideProgressHud()
        refreshControl.endRefreshing()
        tableView.reloadData()
    }
}

//MARK: - UITableViewDelegate & UITableViewDataSource

extension MainViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.rowSelected(at: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRowsInSection()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(cell: NewsTableViewCell.self, for: indexPath)
        if let presenter = presenter.presenterForCell(at: indexPath) as? NewsTableViewCellOutput {
            cell.presenter = presenter
        }
        return cell
    }
}

//MARK: - Configuration

private extension MainViewController {

    func configureAppearance() {
        setupTableViews()
        configureNavigationBar()
        setupRefreshControll()
    }

    func setupTableViews() {
        tableView.registerWithNib(cellType: NewsTableViewCell.self)
    }

    func configureNavigationBar() {
        title = Constants.navBarTitle
    }

    func setupRefreshControll() {
        tableView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(refreshRSSData(_:)), for: .valueChanged)
    }

    @objc func refreshRSSData(_ sender: Any) {
        presenter.pullToRefresh()
    }
}
