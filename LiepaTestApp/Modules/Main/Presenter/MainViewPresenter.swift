import Foundation

final class MainViewPresenter: MainViewOutput, MainViewInteractorOutput {

    // MARK: - Property list

    weak var view: MainViewInput!
    var interactor: MainViewInteractorInput!
    var router: MainViewRouterInput!

    private var newsPresentersArray = [NewsTableViewCellPresenter]()

    // MARK: - MainViewOutput

    func viewDidLoad() {
        interactor.getNews()
    }

    func pullToRefresh() {
        interactor.getNews()
    }

    func numberOfRowsInSection() -> Int {
        return newsPresentersArray.count
    }

    func rowSelected(at indexPath: IndexPath) {
        let cellPresenter = newsPresentersArray[indexPath.row]
        cellPresenter.selected()
        router.openDetails(with: cellPresenter.getModel())
    }

    func presenterForCell(at indexPath: IndexPath) -> CellOutputProtocol {
        return newsPresentersArray[indexPath.row]
    }

    // MARK: - MainViewInteractorOutput

    func successGetNews(_ news: [News]) {
        newsPresentersArray = news.map(NewsTableViewCellPresenter.init)
        view.reloadTable()
    }

    func failureGetNews(_ errors: [Error]?) {
        //TODO: - Обработка ошибок
    }
}
