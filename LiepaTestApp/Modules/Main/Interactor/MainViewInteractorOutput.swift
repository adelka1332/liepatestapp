protocol MainViewInteractorOutput: class {
    func successGetNews(_ news: [News])
    func failureGetNews(_ errors: [Error]?)
}
