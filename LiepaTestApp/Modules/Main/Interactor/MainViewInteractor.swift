import Foundation
import SWXMLHash

final class MainViewInteractor: MainViewInteractorInput {

    //MARK: - Constants

    private enum Constants {
        static let lentaru = SettingsService.FeedSources.lentaru.rawValue
        static let gazetaru = SettingsService.FeedSources.gazetaru.rawValue
    }

    //MARK: - Type Aliases

    private typealias EmptyBlock = () -> Void
    private typealias LentaBlock = ([Lenta]) -> Void
    private typealias GazetaBlock = ([Gazeta]) -> Void
    private typealias ErrorBlock = (Error) -> Void

    // MARK: - Property list

    weak var presenter: MainViewInteractorOutput!
    var apiManager: ApiManagerRSS!
    var dispatchGroup: DispatchGroup!

    private var latestNews = [String:[News]]()
    private var errors: [Error]?

    //MARK: - MainViewInteractorInput

    func getNews() {

        guard !SettingsService.feedSources.isEmpty else {
            getLentaNews()
            getGazetaNews()
            return
        }

        typealias feedTypes = SettingsService.FeedSources

        for source in SettingsService.feedSources {
            if source == feedTypes.gazetaru.rawValue { getGazetaNews() }
            if source == feedTypes.lentaru.rawValue { getLentaNews() }
        }
        dispatchGroup.notify(qos: .userInitiated, queue: .main, execute: notifyRequestFinished)
    }

    //MARK: - Private Methods

    private func getLentaNews() {
        dispatchGroup.enter()
        apiManager.getLentaNews(success: successGetLentaNews,
                                failure: requestFailure)
    }

    private func getGazetaNews() {
        dispatchGroup.enter()
        apiManager.getGazetaNews(success: successGetGazetaNews,
                                 failure: requestFailure)
    }

    private lazy var successGetLentaNews: LentaBlock = { [weak self] news in
        guard let self = self else { return }

        let lentaNewsMap = news.map { element in
            News(title: element.title,
                 author: element.author,
                 description: element.description,
                 pubDate: element.pubDate,
                 imageUrl: element.imageUrl)
        }

        self.latestNews[Constants.lentaru] = lentaNewsMap

        self.dispatchGroup.leave()
    }

    private lazy var successGetGazetaNews: GazetaBlock = { [weak self] news in
        guard let self = self else { return }

        let gazetaNewsMap = news.map { element in
            News(title: element.title,
                 author: element.author,
                 description: element.description,
                 pubDate: element.pubDate,
                 imageUrl: element.imageUrl)
        }

        self.latestNews[Constants.gazetaru] = gazetaNewsMap

        self.dispatchGroup.leave()
    }

    private lazy var requestFailure: ErrorBlock = { [weak self] error in
        guard let self = self else { return }

        self.errors == nil ? (self.errors = [error]) : (self.errors?.append(error))
        self.dispatchGroup.leave()
    }

    private lazy var notifyRequestFinished: EmptyBlock = { [weak self] in
        guard let self = self else { return }

        guard !self.latestNews.isEmpty else {
            self.presenter.failureGetNews(self.errors)
            return
        }

        if self.latestNews.count > 1 {
            let sortedNews = self.sortingNews(self.latestNews)
            self.presenter.successGetNews(sortedNews)
        } else {
            let news = self.latestNews.values.reduce([], +)
            self.presenter.successGetNews(news)
        }
    }

    private func sortingNews(_ news: [String:[News]]) -> [News] {
        return news.values.reduce([], +).sorted(by: { $0.pubDate > $1.pubDate })
    }
}
/*
    private weak var updateTimer: Timer?
    private func createTimer() {
        if updateTimer == nil {
            let timer = Timer(timeInterval: 10.0,
                              target: self,
                              selector: #selector(updateForTimer),
                              userInfo: nil,
                              repeats: true)
            RunLoop.current.add(timer, forMode: .common)
            timer.tolerance = 0.1
            self.updateTimer = timer
        }
     }

     @objc private func updateForTimer() {
         getNews()
     }
*/
