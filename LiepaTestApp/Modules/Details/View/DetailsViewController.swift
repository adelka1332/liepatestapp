import UIKit
import Kingfisher

final class DetailsViewController: UIViewController, DetailsViewInput {

    private enum Constants {
        static let dateFormat = "d MMMM, H:mm"
    }

    @IBOutlet private weak var headingLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var newsImage: UIImageView!
    @IBOutlet private weak var newsDescriptionLabel: UILabel!

    var presenter: DetailsViewOutput!

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
    }

    func configureView(with model: News) {
        title = model.author
        headingLabel.text = model.title
        dateLabel.text = model.pubDate.toString(format: Constants.dateFormat)
        newsImage.kf.setImage(with: model.imageUrl)
        newsDescriptionLabel.text = model.description
    }
}
