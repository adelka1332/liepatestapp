protocol DetailsViewOutput {
    func viewDidLoad()
    func set(data: News)
}
