protocol DetailsViewInput: class {
    func configureView(with model: News)
}
