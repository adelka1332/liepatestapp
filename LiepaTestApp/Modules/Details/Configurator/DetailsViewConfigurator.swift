import UIKit

final class DetailsViewConfigurator {

    static func configureModule(with viewController: DetailsViewController) {

        let router = DetailsViewRouter()
        router.transitionHandler = viewController

        let interactor = DetailsViewInteractor()

        let presenter = DetailsViewPresenter()
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter

        viewController.presenter = presenter
    }
}
