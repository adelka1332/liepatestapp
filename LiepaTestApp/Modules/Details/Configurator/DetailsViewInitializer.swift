import UIKit

final class DetailsViewInitializer: NSObject {
    
    // MARK: - Property list

    @IBOutlet private weak var detailsViewController: DetailsViewController!

    // MARK: - Overrides

    override func awakeFromNib() {
        DetailsViewConfigurator.configureModule(with: detailsViewController)
    }
}
