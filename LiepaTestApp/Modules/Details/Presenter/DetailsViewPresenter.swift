import Foundation

final class DetailsViewPresenter {
    
    // MARK: - Property list

    weak var view: DetailsViewInput!
    var interactor: DetailsViewInteractorInput!
    var router: DetailsViewRouterInput!

    var data: News!
}

//MARK: - DetailsViewOutput

extension DetailsViewPresenter: DetailsViewOutput {

    func viewDidLoad() {
        view.configureView(with: data)
    }

    func set(data: News) {
        self.data = data
    }
}

//MARK: - DetailsViewInteractorOutput

extension DetailsViewPresenter: DetailsViewInteractorOutput {
    
}
