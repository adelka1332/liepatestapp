import Foundation

final class DetailsViewInteractor {
    
    // MARK: - Property list

    weak var presenter: DetailsViewInteractorOutput!
}

//MARK: - DetailsViewInteractorInput

extension DetailsViewInteractor: DetailsViewInteractorInput {
    
}
